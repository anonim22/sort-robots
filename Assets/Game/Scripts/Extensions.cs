using System.Collections.Generic;

namespace EnglishKids {
	public static class Extensions {
		public static void Shuffle<T>(this IList<T> list)
		{
			var count = list.Count;
			var last  = count - 1;
			for (var i = 0; i < last; ++i) {
				var r   = UnityEngine.Random.Range(i, count);
				var tmp = list[i];
				list[i] = list[r];
				list[r] = tmp;
			}
		}
	}
}