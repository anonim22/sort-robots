using System;
using System.Collections.Generic;
using UnityEngine;

namespace EnglishKids.Sounds.Levels {
	public abstract class LevelSounds : MonoBehaviour {
		public virtual LevelType Level => LevelType.SortRobots;


		public virtual Dictionary<int, SoundManager.Sound> GetSounds() {
			return null;
		}


		public enum LevelType {
			SortRobots,
		}
	}
}