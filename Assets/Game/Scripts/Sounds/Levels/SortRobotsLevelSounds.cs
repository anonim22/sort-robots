using System;
using System.Collections.Generic;

namespace EnglishKids.Sounds.Levels {
	public class SortRobotsLevelSounds : LevelSounds {
		public override LevelType         Level => LevelType.SortRobots;
		public          List<SoundObject> Sounds = new List<SoundObject>();


		public override Dictionary<int, SoundManager.Sound> GetSounds() {
			var dictionary = new Dictionary<int, SoundManager.Sound>();

			foreach (var soundObject in Sounds) {
				var index = (int) soundObject.Name;
				dictionary.Add(index, soundObject.Sound);
			}

			return dictionary;
		}


		[Serializable]
		public struct SoundObject {
			public string             InspectorName;
			public SoundName          Name;
			public SoundManager.Sound Sound;
		}

		[Serializable]
		public enum SoundName {
			CorrectAnswer,
			WrongAnswer,
			NewParts,
			Pick,
			Robot1,
			Robot2,
			Robot3,
			Robot4,
			Background,

			// Voice colors
			Black,
			Blue,
			Brown,
			Gray,
			Green,
			Orange,
			Pink,
			Purple,
			Red,
			White,
			Yellow,
		}
	}
}