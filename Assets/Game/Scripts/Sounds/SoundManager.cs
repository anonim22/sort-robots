using System;
using System.Collections.Generic;
using EnglishKids.Sounds.Levels;
using UnityEngine;

namespace EnglishKids.Sounds {
	public class SoundManager : MonoBehaviour {
		private const  int          MaximumSoundsInQueue = 1;
		private static SoundManager Instance;

		[SerializeField] private LevelSounds LevelSounds;

		private Dictionary<int, Sound>       _sounds       = new Dictionary<int, Sound>();

		private Dictionary<SoundGroup, List<Sound>> _soundGroups       = new Dictionary<SoundGroup, List<Sound>>();
		private Dictionary<SoundGroup, AudioSource> _groupAudioSources = new Dictionary<SoundGroup, AudioSource>();

		// AudioSources Pool
		private List<AudioSource> _freeAudioSources = new List<AudioSource>();
		private List<AudioSource> _busyAudioSources = new List<AudioSource>();


		private void Awake() {
			Instance = this;
			_sounds  = LevelSounds.GetSounds();
			InitializeGroups();
		}


		private void Update() {
			CheckGroupQueue();
			CheckSoundsPool();
		}


		/// <summary>
		/// Созает группы для каждого из значений SoundGroup
		/// </summary>
		private void InitializeGroups() {
			var groups = Enum.GetValues(typeof(SoundGroup));
			foreach (var value in groups) {
				var group = (SoundGroup) value;
				_soundGroups.Add(group, new List<Sound>());
				_groupAudioSources.Add(group, CreateAudioSource());
			}
		}


		/// <summary>
		/// Проверка очереди воспроизведения для группы
		/// </summary>
		private void CheckGroupQueue() {
			foreach (var groupSource in _groupAudioSources) {
				var group       = groupSource.Key;
				var audioSource = groupSource.Value;

				if (!audioSource.isPlaying) {
					var soundsQueue = _soundGroups[group];
					if (soundsQueue.Count > 0) {
						var newSound = soundsQueue[0];
						soundsQueue.RemoveAt(0);

						audioSource.clip   = newSound.Clip;
						audioSource.volume = GetVolume(newSound);
						audioSource.Play();
					}
				}
			}
		}


		/// <summary>
		/// Помещает в пул отыгравшие источники
		/// </summary>
		private void CheckSoundsPool() {
			for (var i = 0; i < _busyAudioSources.Count; i++) {
				var audioSource = _busyAudioSources[i];
				if (!audioSource.isPlaying) {
					_busyAudioSources.RemoveAt(i--);
					_freeAudioSources.Add(audioSource);
				}
			}
		}


		/// <summary>
		/// Обычное проигрывание звука. Использует свободный источник из пула или создает новый
		/// </summary>
		public static void Play<T>(T soundType, bool loop = false) where T : Enum {
			var index = (int) (object) soundType;

			if (!Instance._sounds.ContainsKey(index)) {
				throw new IndexOutOfRangeException($"Cannot found sound {typeof(T)}.{soundType}");
			}

			var sound       = Instance._sounds[index];
			var audioSource = Instance.GetFromPool();
			audioSource.clip   = sound.Clip;
			audioSource.volume = GetVolume(sound);
			audioSource.loop   = loop;
			audioSource.Play();
		}


		/// <summary>
		/// Помещает звук в группу, где звуки воспроизводятся поочередно. При переполнении очереди, старые удаляются
		/// </summary>
		public static void PlayInGroup<T>(T soundType, SoundGroup soundGroup) where T : Enum {
			var index = (int) (object) soundType;

			if (!Instance._sounds.ContainsKey(index)) {
				throw new IndexOutOfRangeException($"Cannot found sound {typeof(T)}.{soundType}");
			}

			var sound         = Instance._sounds[index];
			var soundsInGroup = Instance._soundGroups[soundGroup];
			if (soundsInGroup.Count >= MaximumSoundsInQueue) {
				soundsInGroup.RemoveAt(0);
			}
			soundsInGroup.Add(sound);
		}


		private AudioSource GetFromPool() {
			if (_freeAudioSources.Count > 0) {
				var source = _freeAudioSources[0];
				_freeAudioSources.RemoveAt(0);
				return source;
			}

			var newSource = CreateAudioSource();
			_busyAudioSources.Add(newSource);
			return newSource;
		}


		private static AudioSource CreateAudioSource() {
			var audioGameObject = new GameObject("AudioSource");
			var audioSource     = audioGameObject.AddComponent<AudioSource>();

			audioGameObject.transform.SetParent(Instance.transform);
			return audioSource;
		}


		private static float GetVolume(Sound sound) {
			var globalVolume = 1f;
			switch (sound.Type) {
				case Sound.SoundType.Effects:
					globalVolume = SoundSettings.EffectsVolume;
					break;
				case Sound.SoundType.Voice:
					globalVolume = SoundSettings.VoiceVolume;
					break;
				case Sound.SoundType.Music:
					globalVolume = SoundSettings.MusicVolume;
					break;
			}

			var soundVolume = globalVolume * sound.Volume;
			return soundVolume;
		}


		[Serializable]
		public struct Sound {
			public AudioClip Clip;
			public SoundType Type;
			public float     Volume;

			public enum SoundType {
				Effects,
				Voice,
				Music
			}
		}

		public enum SoundGroup {
			Voice,
		}
	}
}