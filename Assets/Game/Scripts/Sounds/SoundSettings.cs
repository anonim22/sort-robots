namespace EnglishKids.Sounds {
	public static class SoundSettings {
		public static float EffectsVolume = 1f;
		public static float VoiceVolume   = 1f;
		public static float MusicVolume   = 1f;
	}
}