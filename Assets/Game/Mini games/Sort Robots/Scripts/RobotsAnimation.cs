using System;
using EnglishKids.SortRobots.Data;
using EnglishKids.Sounds;
using EnglishKids.Sounds.Levels;
using Spine.Unity;
using UnityEngine;

namespace EnglishKids.SortRobots {
	public class RobotsAnimation : MonoBehaviour {
		[SerializeField] private Robot Robot1, Robot2, Robot3, Robot4;


		public void ShowRobot(RobotType robotType, Transform shadowParent, GameVariant.Colors color, bool isLeftSide) {
			var robot = GetRobot(robotType);

			robot.RobotSkeleton.gameObject.SetActive(true);
			robot.RobotSkeleton.Skeleton.SetSkin(GetRobotSkin(color));
			robot.RobotSkeleton.state.SetAnimation(0, "action", false);
			robot.RobotSkeleton.Skeleton.SetSlotsToSetupPose();
			robot.RobotSkeleton.LateUpdate();
			robot.RobotSkeleton.transform.SetParent(shadowParent);

			var localPosition                = robot.LocalPosition;
			if (!isLeftSide) localPosition.x *= -1;

			robot.RobotSkeleton.transform.localPosition = localPosition;
			SoundManager.Play(robot.Sound);
		}


		public void HideAll() {
			Robot1.RobotSkeleton.gameObject.SetActive(false);
			Robot2.RobotSkeleton.gameObject.SetActive(false);
			Robot3.RobotSkeleton.gameObject.SetActive(false);
			Robot4.RobotSkeleton.gameObject.SetActive(false);
		}


		private Robot GetRobot(RobotType robotType) {
			switch (robotType) {
				default:               return Robot1;
				case RobotType.Robot2: return Robot2;
				case RobotType.Robot3: return Robot3;
				case RobotType.Robot4: return Robot4;
			}
		}


		private string GetRobotSkin(GameVariant.Colors color) {
			switch (color) {
				case GameVariant.Colors.Black:  return "black";
				case GameVariant.Colors.Blue:   return "blue";
				case GameVariant.Colors.Brown:  return "brown";
				case GameVariant.Colors.Gray:   return "gray";
				case GameVariant.Colors.Green:  return "greed";
				case GameVariant.Colors.Orange: return "orange";
				case GameVariant.Colors.Pink:   return "pink";
				case GameVariant.Colors.Purple: return "purple";
				case GameVariant.Colors.Red:    return "red";
				case GameVariant.Colors.White:  return "white";
				default:                        return "yellow";
			}
		}


		public enum RobotType {
			Robot1,
			Robot2,
			Robot3,
			Robot4
		}

		[Serializable]
		public struct Robot {
			public SkeletonAnimation               RobotSkeleton;
			public Vector2                         LocalPosition;
			public SortRobotsLevelSounds.SoundName Sound;
		}
	}
}