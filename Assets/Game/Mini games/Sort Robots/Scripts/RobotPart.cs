using System;
using EnglishKids.SortRobots.Data;
using EnglishKids.Sounds;
using EnglishKids.Sounds.Levels;
using UnityEngine;
using UnityEngine.EventSystems;

namespace EnglishKids.SortRobots {
	[RequireComponent(typeof(ConveyorObject))]
	public class RobotPart : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
		/// <summary>
		/// Максимальная дистанция до точки установки. По другому "примагничивание"
		/// </summary>
		private const float MaxPlaceDistance = 0.3f;
		private const float ColliderSizeMultiplier = 1.125f;

		public ConveyorObject ConveyorObject { get; private set; }

		public bool                        Placed { get; private set; }
		public event Action<RobotPartData> OnPartPlaced;

		private Transform     _transform;
		private RobotPartData _partData;
		private Transform     _shadow;


		private void Awake() {
			_transform     = transform; // Кэширование Transform
			ConveyorObject = GetComponent<ConveyorObject>();
		}


		/// <summary>
		/// Установка информации о детали и настройка
		/// </summary>
		public void SetPartData(RobotPartData partData) {
			_partData = partData;

			var spriteRenderer = GetComponent<SpriteRenderer>();
			spriteRenderer.sprite       = partData.Sprite;
			spriteRenderer.sortingOrder = partData.OrderInLayer;

			var boxCollider = gameObject.AddComponent<BoxCollider2D>();
			boxCollider.size *= ColliderSizeMultiplier;

			ConveyorObject.Size = partData.Size;
		}


		public void SetShadowObject(Transform shadowObject) {
			_shadow = shadowObject;
		}


		#region IDragHandlers

		public void OnBeginDrag(PointerEventData eventData) {
			if (Placed) return;

			// Удаляем деталь с конвейера при ее перемещении
			ConveyorObject.Conveyor.RemoveObject(ConveyorObject);
			SoundManager.Play(SortRobotsLevelSounds.SoundName.Pick);
		}


		public void OnDrag(PointerEventData eventData) {
			if (Placed) return;

			MovePart(eventData.position);
		}


		public void OnEndDrag(PointerEventData eventData) {
			if (Placed) return;

			// Размещение детали или возвращение на конвейер
			if (CanPlace()) {
				PlacePart();
				SoundManager.Play(SortRobotsLevelSounds.SoundName.CorrectAnswer);
			}
			else {
				GameController.Instance.Conveyor.PutObject(ConveyorObject);
				SoundManager.Play(SortRobotsLevelSounds.SoundName.WrongAnswer);
			}
		}

		#endregion


		private void MovePart(Vector2 screenPosition) {
			var worldPosition = Camera.main.ScreenToWorldPoint(screenPosition);
			worldPosition.z     = _transform.position.z;
			_transform.position = worldPosition;
		}


		private bool CanPlace() {
			var partPosition       = _transform.position;
			var placePosition      = _partData.SocketPosition;
			var worldPlacePosition = _shadow.TransformPoint(placePosition);

			var distance = Vector2.Distance(partPosition, worldPlacePosition);
			return distance < MaxPlaceDistance;
		}


		private void PlacePart() {
			_transform.SetParent(_shadow);
			_transform.localPosition = _partData.SocketPosition;

			Placed = true;
			OnPartPlaced?.Invoke(_partData);
		}
	}
}