using System;
using System.Collections.Generic;
using System.Linq;
using EnglishKids.SortRobots.Data;
using EnglishKids.Sounds;
using EnglishKids.Sounds.Levels;
using Spine.Unity;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace EnglishKids.SortRobots {
	public class GameController : MonoBehaviour {
		public static GameController Instance { get; private set; }

		[SerializeField] private List<GameVariant> GameVariants = new List<GameVariant>();

		[Header("Background")]
		[SerializeField] private SetupObjects LeftSetupObjects;
		[SerializeField] private SetupObjects RightSetupObjects;

		[Space] [SerializeField] private RobotPart         RobotPartPrefab;
		[SerializeField]         private SkeletonAnimation StarsAnimation;

		// public get; private set;
		[Space] [SerializeField] private Conveyor        _Conveyor;
		public                           Conveyor        Conveyor => _Conveyor;
		[SerializeField] private         RobotsAnimation RobotsAnimation;

		private GameVariant _leftGameVariant,     _rightGameVariant;
		private GameVariant _previousGameVariant, _previousGameVariant2;

		private List<RobotPart> _leftRobotParts  = new List<RobotPart>();
		private List<RobotPart> _rightRobotParts = new List<RobotPart>();

		private bool _leftRobotAnimationsPlayed, _rightRobotAnimationsPlayed;


		private void Awake() {
			Instance = this;
		}


		private void Start() {
			NewGame();

			LeftSetupObjects.VoiceButton.onClick.AddListener(() => OnVoiceButton(true));
			RightSetupObjects.VoiceButton.onClick.AddListener(() => OnVoiceButton(false));

			SoundManager.Play(SortRobotsLevelSounds.SoundName.Background, true);
		}


		private void NewGame() {
			GenerateGameVariants();

			SetupVariants(_leftGameVariant,  LeftSetupObjects);
			SetupVariants(_rightGameVariant, RightSetupObjects);

			CreateRobotParts();
			RobotsAnimation.HideAll();

			_leftRobotAnimationsPlayed       = _rightRobotAnimationsPlayed = false;
			LeftSetupObjects.Shadow.enabled  = true;
			RightSetupObjects.Shadow.enabled = true;
		}


		/// <summary>
		/// Рандомный выбор роботов
		/// </summary>
		private void GenerateGameVariants() {
			_previousGameVariant  = _leftGameVariant;
			_previousGameVariant2 = _rightGameVariant;

			// Удаляем предыдущие 2 варианта, чтобы они не попались второй раз подряд
			var gameVariants = new List<GameVariant>(GameVariants);
			gameVariants.Remove(_previousGameVariant);
			gameVariants.Remove(_previousGameVariant2);

			_leftGameVariant = gameVariants[Random.Range(0, gameVariants.Count)];
			gameVariants.Remove(_leftGameVariant);
			_rightGameVariant = gameVariants[Random.Range(0, gameVariants.Count)];

			DestroyLeftRobotParts();
			DestroyRightRobotParts();
		}


		private void DestroyLeftRobotParts() {
			_leftRobotParts.ForEach(r => Destroy(r.gameObject));
			_leftRobotParts.Clear();
		}


		private void DestroyRightRobotParts() {
			_rightRobotParts.ForEach(r => Destroy(r.gameObject));
			_rightRobotParts.Clear();
		}


		/// <summary>
		/// Настройка фона и тени собираемого робота
		/// </summary>
		private void SetupVariants(GameVariant gameVariant, SetupObjects setupObjects) {
			setupObjects.Background.sprite = gameVariant.Background;
			setupObjects.Shadow.sprite     = gameVariant.RobotShadow;
			setupObjects.ColorText.text    = gameVariant.Color.ToString();
			setupObjects.VoiceButton.gameObject.SetActive(false);
		}


		/// <summary>
		/// Создание деталей роботов и размещение их на ковейере
		/// </summary>
		private void CreateRobotParts() {
			var leftParts  = new List<RobotPartData>(_leftGameVariant.RobotParts);
			var rightParts = new List<RobotPartData>(_rightGameVariant.RobotParts);

			while (leftParts.Count > 0 || rightParts.Count > 0) {
				var isLeftSide = Random.Range(0, 2) == 0;           // Выбираем случайную сторону
				if (leftParts.Count       == 0) isLeftSide = false; // Выбираем правую сторону, если слева детали закончились
				else if (rightParts.Count == 0) isLeftSide = true;  // Выбираем левую сторону, если справа детали закончились

				var shadow = isLeftSide ? LeftSetupObjects.Shadow.transform : RightSetupObjects.Shadow.transform;
				var list   = isLeftSide ? leftParts : rightParts;

				// Достаем случайную деталь
				var partIndex = Random.Range(0, list.Count);
				var partData  = list[partIndex];
				list.RemoveAt(partIndex);

				// Создаем ее и помещаем в список всех частей
				var robotPart = CreateRobotPart(partData, shadow);
				if (isLeftSide) _leftRobotParts.Add(robotPart);
				else _rightRobotParts.Add(robotPart);
			}
		}


		private RobotPart CreateRobotPart(RobotPartData partData, Transform shadowObject) {
			var robotPart = Instantiate(RobotPartPrefab);
			robotPart.SetPartData(partData);
			robotPart.SetShadowObject(shadowObject);
			robotPart.OnPartPlaced += OnPartPlaced;

			_Conveyor.PutObject(robotPart.ConveyorObject);
			return robotPart;
		}


		/// <summary>
		/// Вызывается после установки каждой части на свое место
		/// </summary>
		private void OnPartPlaced(RobotPartData partData) {
			// Воспроизведение анимации звездочки
			StarsAnimation.gameObject.SetActive(true);
			StarsAnimation.state.SetAnimation(0, "correct_answer", false);

			// Озвучка цвета установленной детали
			if (_leftGameVariant.RobotParts.Contains(partData)) {
				var colorSound = _leftGameVariant.ColorToSoundName();
				SoundManager.PlayInGroup(colorSound, SoundManager.SoundGroup.Voice);
				LeftSetupObjects.VoiceButton.gameObject.SetActive(true);
			}
			else if (_rightGameVariant.RobotParts.Contains(partData)) {
				var colorSound = _rightGameVariant.ColorToSoundName();
				SoundManager.PlayInGroup(colorSound, SoundManager.SoundGroup.Voice);
				RightSetupObjects.VoiceButton.gameObject.SetActive(true);
			}

			var leftSideCompleted  = SideCompleted(_leftRobotParts);
			var rightSideCompleted = SideCompleted(_rightRobotParts);

			// Воспроизведение анимации робота
			if (leftSideCompleted && !_leftRobotAnimationsPlayed) {
				DestroyLeftRobotParts();
				RobotsAnimation.ShowRobot(_leftGameVariant.Robot, LeftSetupObjects.Shadow.transform, _leftGameVariant.Color, true);
				_leftRobotAnimationsPlayed      = true;
				LeftSetupObjects.Shadow.enabled = false;
			}

			if (rightSideCompleted && !_rightRobotAnimationsPlayed) {
				DestroyRightRobotParts();
				RobotsAnimation.ShowRobot(_rightGameVariant.Robot, RightSetupObjects.Shadow.transform, _rightGameVariant.Color, true);
				_rightRobotAnimationsPlayed      = true;
				RightSetupObjects.Shadow.enabled = false;
			}

			// Начало новой игры
			if (leftSideCompleted && rightSideCompleted) Invoke(nameof(NewGame), 3f);
		}


		private bool SideCompleted(List<RobotPart> robotParts) {
			return robotParts.All(p => p.Placed);
		}


		private void OnVoiceButton(bool isLeft) {
			var gameVariant = isLeft ? _leftGameVariant : _rightGameVariant;
			var sound       = gameVariant.ColorToSoundName();
			SoundManager.PlayInGroup(sound, SoundManager.SoundGroup.Voice);
		}


		[Serializable]
		public struct SetupObjects {
			public SpriteRenderer  Background;
			public SpriteRenderer  Shadow;
			public Button          VoiceButton;
			public TextMeshProUGUI ColorText;
		}
	}
}