using UnityEngine;

namespace EnglishKids.SortRobots {
	public class ConveyorSocket : MonoBehaviour {
		/// <summary>
		/// Объект содержащийся в данном сокете
		/// </summary>
		[HideInInspector] public     ConveyorObject ConveyorObject;

		// Кэширование Transform
		[HideInInspector] public new Transform      transform;


		private void Awake() {
			transform = GetComponent<Transform>();
		}
	}
}