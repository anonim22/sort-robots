﻿using System.Collections.Generic;
using System.Linq;
using EnglishKids.Sounds;
using EnglishKids.Sounds.Levels;
using UnityEngine;

namespace EnglishKids.SortRobots {
	public class Conveyor : MonoBehaviour {
		[SerializeField] private Material             ConveyorMaterial;
		[SerializeField] private float                Speed         = 1f;
		[SerializeField] private List<ConveyorSocket> ObjectSockets = new List<ConveyorSocket>();

		[Tooltip("Верхняя точка, после которой начинается игровое пространство")]
		[SerializeField] private float UpperVisibleBorder;
		[Tooltip("Верхняя точка, в которую перемещаются сокеты, после полного опускания")]
		[SerializeField] private float UpperBorder;
		[Tooltip("Нижняя точка, после которой сокеты с объектами останавливают конвейер")]
		[SerializeField] private float LowerVisibleBorder;
		[Tooltip("Нижняя точка, после которой сокеты перемещаются наверх")]
		[SerializeField] private float LowerBorder;

		private bool                  _isRunning  = true;
		private Queue<ConveyorObject> _partsQueue = new Queue<ConveyorObject>();


		private void Update() {
			CheckStopConveyor();
			if (_isRunning) {
				MoveConveyor();
				MoveSockets();
				CheckQueue();
			}
		}


		/// <summary>
		/// Помещение объекта в очередь
		/// </summary>
		public void PutObject(ConveyorObject conveyorObject) {
			_partsQueue.Enqueue(conveyorObject);
			conveyorObject.Conveyor = this;
			conveyorObject.gameObject.SetActive(false);
			conveyorObject.OnPutConveyor();
		}


		/// <summary>
		/// Удаление объекта из очереди
		/// </summary>
		public void RemoveObject(ConveyorObject conveyorObject) {
			foreach (var objectSocket in ObjectSockets) {
				if (objectSocket.ConveyorObject != conveyorObject) continue;

				objectSocket.ConveyorObject = null;
				conveyorObject.Conveyor     = null;
				conveyorObject.transform.SetParent(null);
				conveyorObject.OnRemoveFromConveyor();
				return;
			}
		}


		private void MoveConveyor() {
			var offset = ConveyorMaterial.mainTextureOffset;
			offset.y                           += Time.deltaTime * Speed;
			offset.y                           -= offset.y > 1 ? 1 : 0;
			ConveyorMaterial.mainTextureOffset =  offset;
		}


		private void MoveSockets() {
			foreach (var objectSocket in ObjectSockets) {
				var objectPosition = objectSocket.transform.position;
				objectPosition.y                -= Time.deltaTime * Speed * 10f;
				objectPosition.y                =  objectPosition.y < LowerBorder ? UpperBorder : objectPosition.y;
				objectSocket.transform.position =  objectPosition;
			}
		}


		/// <summary>
		/// Проверка наличия доступных объектов в очереди и свободных сокетов(мест размещения)
		/// </summary>
		private void CheckQueue() {
			if (_partsQueue.Count < 1) return;

			var freeSocket = ObjectSockets.FirstOrDefault(s => s.ConveyorObject == null && s.transform.position.y > UpperVisibleBorder);
			if (!freeSocket) return;

			var conveyorObject  = _partsQueue.Dequeue();
			var objectTransform = conveyorObject.transform;

			freeSocket.ConveyorObject = conveyorObject;

			objectTransform.SetParent(freeSocket.transform);
			objectTransform.localPosition = Vector3.zero;
			objectTransform.gameObject.SetActive(true);
		}


		/// <summary>
		/// Если сокеты с объектами дошли до конца ленты, то конвейер останавливается
		/// </summary>
		private void CheckStopConveyor() {
			var lowerSockets = ObjectSockets.Where(s => s.transform.position.y <  LowerVisibleBorder).ToList();
			if (!lowerSockets.Any()) return;
			var anyObject = lowerSockets.Any(s => s.ConveyorObject);

			// Проигрываем звук, если состояние изменилось
			if (_isRunning != !anyObject) {
				SoundManager.Play(SortRobotsLevelSounds.SoundName.NewParts);
			}

			_isRunning = !anyObject;
		}
	}
}