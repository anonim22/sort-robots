using System;
using UnityEngine;

namespace EnglishKids.SortRobots.Data {
	[Serializable]
	public class RobotPartData {
		public string                    Name;
		public Sprite                    Sprite;
		public Vector2                   SocketPosition;
		public ConveyorObject.ObjectSize Size;
		public int                       OrderInLayer;
	}
}