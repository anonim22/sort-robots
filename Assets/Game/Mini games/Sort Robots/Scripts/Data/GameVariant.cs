using System.Collections.Generic;
using EnglishKids.Sounds.Levels;
using UnityEngine;

namespace EnglishKids.SortRobots.Data {
	[CreateAssetMenu(fileName = "NewGameVariant", menuName = "New Game Variant", order = 0)]
	public class GameVariant : ScriptableObject {
		public string                    Name;
		public Sprite                    Background;
		public Sprite                    RobotShadow;
		public List<RobotPartData>       RobotParts = new List<RobotPartData>();
		public Colors                    Color;
		public RobotsAnimation.RobotType Robot;


		public SortRobotsLevelSounds.SoundName ColorToSoundName() {
			switch (Color) {
				case Colors.Black:  return SortRobotsLevelSounds.SoundName.Black;
				case Colors.Blue:   return SortRobotsLevelSounds.SoundName.Blue;
				case Colors.Brown:  return SortRobotsLevelSounds.SoundName.Brown;
				case Colors.Gray:   return SortRobotsLevelSounds.SoundName.Gray;
				case Colors.Green:  return SortRobotsLevelSounds.SoundName.Green;
				case Colors.Orange: return SortRobotsLevelSounds.SoundName.Orange;
				case Colors.Pink:   return SortRobotsLevelSounds.SoundName.Pink;
				case Colors.Purple: return SortRobotsLevelSounds.SoundName.Purple;
				case Colors.Red:    return SortRobotsLevelSounds.SoundName.Red;
				case Colors.White:  return SortRobotsLevelSounds.SoundName.White;
				case Colors.Yellow: return SortRobotsLevelSounds.SoundName.Yellow;
			}

			return SortRobotsLevelSounds.SoundName.Black;
		}


		public enum Colors {
			Black,
			Blue,
			Brown,
			Gray,
			Green,
			Orange,
			Pink,
			Purple,
			Red,
			White,
			Yellow,
		}
	}
}