using DG.Tweening;
using UnityEngine;

namespace EnglishKids.SortRobots {
	public class ConveyorObject : MonoBehaviour {
		[HideInInspector] public  Conveyor Conveyor;
		[SerializeField]  private float    ResizeSpeed = 0.5f;
		[SerializeField]  private Vector2  OnConveyorScale, DefaultScale;

		/// <summary>
		/// Влияет на Scale при размещении на ковейере
		/// </summary>
		[HideInInspector] public ObjectSize Size;


		public void OnPutConveyor() {
			var randomAngle = Random.Range(-25f, 25f);
			transform.rotation = Quaternion.AngleAxis(randomAngle, Vector3.forward);

			if (Size == ObjectSize.Big) transform.localScale = OnConveyorScale;
		}


		public void OnRemoveFromConveyor() {
			transform.DORotate(Vector3.zero, ResizeSpeed);
			if (Size == ObjectSize.Big) transform.DOScale(DefaultScale, ResizeSpeed);
		}


		public enum ObjectSize {
			Small,
			Big
		}
	}
}